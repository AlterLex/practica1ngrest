#ifndef USUARIO_H
#define USUARIO_H

#include <iostream>
#include <string>
//#include <ctime>
#include "Recurso.h"




struct NodoUsuario{
    NodoUsuario *Siguiente;
    std::string Identificador;
    std::string Nombre;
    std::string Correo;
    //time_t FechaIngreso;
    std::string FechaIngreso;
    NodoRecurso *RecursoUsuario;
    };

struct ListaSimpleUsuarios{
    NodoUsuario *Primero;
    NodoUsuario *Ultimo;
    int Cantidad;
    };

class Usuario
{
public:
    ListaSimpleUsuarios *LSU;
    Usuario();
    ~Usuario();
    NodoUsuario *NuevoUsuarioNodo(const std::string Ide,const std::string Nom,const std::string Correo,const std::string Fecha,NodoRecurso *Source);
    std::string AgregarUsuarioLista(NodoUsuario *Nuevo);
    NodoUsuario *UsuarioBuscado(const std::string Ide);


};

#endif // USUARIO_H
