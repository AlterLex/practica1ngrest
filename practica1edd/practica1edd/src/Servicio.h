#ifndef SERVICIO_H
#define SERVICIO_H


#include <string>

struct NodoServicio{
    NodoServicio *Siguiente;
    std::string Sesion;
    std::string Susuario;
    };
    
struct PilaServicio{
    NodoServicio *Cima;
    int Cantidad;
    };


class Servicio
{
public:
    PilaServicio *PS;
    Servicio();
    ~Servicio();
    NodoServicio *NuevoServicio(const std::string UsuIde);
    bool EnPilar(NodoServicio *Nuevo);
    void VaciarPila();
    

};

#endif // SERVICIO_H
