#ifndef RECURSO_H
#define RECURSO_H

//#include <iostream>
#include <string>

struct NodoRecurso{
    NodoRecurso *Siguiente;
    std::string Dominio;
    std::string Contenido;
    };
    
struct ColaCircularRecurso{
    NodoRecurso *Primero;
    NodoRecurso *Ultimo;
    int Cantidad;
    };

class Recurso
{
public:
    ColaCircularRecurso *CCR;
    Recurso();
    ~Recurso();
    NodoRecurso * NuevoRecurso(const std::string Dominio,const std::string Contenido);
    bool AgregarRecursoCola(NodoRecurso * Nuevo);
    bool ColaCircularVacia();
    NodoRecurso *RecursoParaUsuario();
    

};

#endif // RECURSO_H
