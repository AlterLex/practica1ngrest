#include "Recurso.h"

Recurso::Recurso()
{
    CCR=new ColaCircularRecurso();
    CCR->Ultimo=0;
    CCR->Primero=0;
    CCR->Cantidad=0;
}

Recurso::~Recurso()
{
}

NodoRecurso * Recurso::NuevoRecurso(const std::string Dominio,const std::string Contenido)
{
    NodoRecurso *Nuevo=new NodoRecurso();
    Nuevo->Dominio=Dominio;
    Nuevo->Siguiente=0;
    Nuevo->Contenido=Contenido;
    return Nuevo;
}

bool Recurso::AgregarRecursoCola(NodoRecurso * Nuevo)
{
    if(CCR->Cantidad==0){
        Nuevo->Siguiente=Nuevo;
        CCR->Ultimo=CCR->Primero=Nuevo;
        CCR->Cantidad++;
        return true;
    }
    else{
        Nuevo->Siguiente=CCR->Primero;
        CCR->Ultimo->Siguiente=Nuevo;
        CCR->Ultimo=Nuevo;
        CCR->Cantidad++;
        return true;
        }
}

bool Recurso::ColaCircularVacia(){
    if (CCR->Cantidad==0) {
        return true;
        }
    else{
        return false;
        }
    }

NodoRecurso * Recurso::RecursoParaUsuario()
{
    NodoRecurso *Retorno=CCR->Primero;
    CCR->Primero=CCR->Primero->Siguiente;
    CCR->Ultimo=Retorno;
    return Retorno;
}
