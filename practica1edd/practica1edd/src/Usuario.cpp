#include "Usuario.h"

Usuario::Usuario()
{
    LSU=new ListaSimpleUsuarios();
    LSU->Primero=0;
    LSU->Ultimo=0;
    LSU->Cantidad=0;
}

Usuario::~Usuario()
{
}

NodoUsuario * Usuario::NuevoUsuarioNodo(const std::string Ide,const std::string Nom,const std::string Correo,const std::string Fecha,NodoRecurso* Source)
{
    NodoUsuario*Nuevo=new NodoUsuario();
    Nuevo->Siguiente=0;
    Nuevo->Identificador=Ide+Source->Dominio;
    Nuevo->Nombre=Nom;
    Nuevo->Correo=Correo;
    
//    const char *time_details = Fecha;
//    struct tm tm;
//    strptime(time_details, "%B %d, %Y, %m", &tm);
//    time_t t = mktime(&tm);
//    Nuevo->FechaIngreso=t;
    Nuevo->FechaIngreso=Fecha;
    Nuevo->RecursoUsuario=Source;
    return Nuevo;
}

std::string Usuario::AgregarUsuarioLista(NodoUsuario *Nuevo)
{
      if(LSU->Cantidad==0){
          LSU->Primero=LSU->Ultimo=Nuevo;
          LSU->Cantidad++;
          return "Primer Ingreso";
          }
    else{
        NodoUsuario *Temp=LSU->Primero;
        while(Temp!=0){
            int Comparacion=Temp->Nombre.compare(Nuevo->Nombre);
            if (Comparacion==0){
                return "Ya existe un usuario llamado asi";
                }
            else if(Comparacion<0){
                if(Temp==LSU->Primero){
                    Nuevo->Siguiente=LSU->Primero;
                    LSU->Primero=Nuevo;
                    LSU->Cantidad++;
                return "Ingresado al inicio";
                }
            else{
                Nuevo->Siguiente=Temp->Siguiente;
                Temp->Siguiente=Nuevo;
                LSU->Cantidad++;
                return "Ingresado en medio";
                }
            }
            else{
                if(Temp==LSU->Ultimo){
                    Temp->Siguiente=Nuevo;
                    LSU->Ultimo=Nuevo;
                    LSU->Cantidad++;
                    return "Ingresado al final";
                    }
                else{
                    Temp=Temp->Siguiente;
                    }
                
                }   
            
            }
        return "No se ingreso caso no tomado";
        }
}

NodoUsuario *  Usuario::UsuarioBuscado(const std::string Ide){
        NodoUsuario *Temp=LSU->Primero;
        
        while(Temp!=0){
            if ((Temp->Identificador.compare(Ide))==0){
                    return Temp;
                }
            else{
                Temp=Temp->Siguiente;
                }
            }
                
        return 0;
    }
